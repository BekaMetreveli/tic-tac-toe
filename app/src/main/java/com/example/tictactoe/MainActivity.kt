package com.example.tictactoe

import android.graphics.drawable.AnimatedVectorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.view.setPadding
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startGameButton.setOnClickListener {
            it.animate().alpha(0f).setDuration(2000).start()
            it.isClickable = false

            myAnimatedGrids.apply {
                setImageResource(R.drawable.anim_grids)
                visibility = View.VISIBLE
                animateView(this)
                isClickable = false
            }

            gridLayout.visibility = View.VISIBLE
        }

    }

    private fun animateView(view: ImageView) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            when (val drawable = view.drawable) {
                is AnimatedVectorDrawableCompat -> {
                    drawable.start()
                }
                is AnimatedVectorDrawable -> {
                    drawable.start()
                }
            }
        }
    }

    //0 for o, 1 fo x
    private var activePlayer = 0
    private var gameState = intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2)
    private val winningPositions = arrayOf(
        intArrayOf(0, 1, 2),
        intArrayOf(3, 4, 5),
        intArrayOf(6, 7, 8),
        intArrayOf(0, 4, 8),
        intArrayOf(2, 4, 6),
        intArrayOf(0, 3, 6),
        intArrayOf(1, 4, 7),
        intArrayOf(2, 5, 8)
    )
    private var gameActive = true
    private var views = intArrayOf(
        R.id.view0,
        R.id.view1,
        R.id.view2,
        R.id.view3,
        R.id.view4,
        R.id.view5,
        R.id.view6,
        R.id.view7,
        R.id.view8
    )

    fun dropIn(view: View) {
        var counter = view as ImageView
        val tappedCounter = counter.tag.toString().toInt()
        views[tappedCounter] = -1
        if (gameActive) {
            gameState[tappedCounter] = activePlayer
            activePlayer = if (activePlayer == 0) {
                counter.setPadding(50)
                counter.setImageResource(R.drawable.anim_x_mark)
                animateView(counter)
                1
            } else {
                counter.setPadding(20)
                counter.setImageResource(R.drawable.anim_o_mark)
                animateView(counter)
                0
            }
            counter.isClickable = false
            for (winningPosition in winningPositions) {
                if (gameState[winningPosition[0]] == gameState[winningPosition[1]] && gameState[winningPosition[1]] == gameState[winningPosition[2]] && gameState[winningPosition[0]] != 2) {
                    var winner = ""
                    winner = if (activePlayer == 1) {
                        "X"
                    } else {
                        "O"
                    }
                    gameActive = false
                    for (v in views) {
                        if (v != -1) {
                            counter = findViewById(v)
                            counter.isClickable = false
                        }
                    }
                    winnerTextView.visibility = View.VISIBLE
                    winnerTextView.text = "$winner has won!"

                    resetButton.visibility = View.VISIBLE
                    resetButton.setOnClickListener {
                        winnerTextView.visibility = View.GONE
                        for (i in 0 until gridLayout.childCount) {
                            val child: ImageView = gridLayout.getChildAt(i) as ImageView
                            child.setImageResource(0)
                            child.isClickable = true
                        }
                        activePlayer = 0
                        gameState = intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2)
                        gameActive = true
                        views = intArrayOf(
                            R.id.view0,
                            R.id.view1,
                            R.id.view2,
                            R.id.view3,
                            R.id.view4,
                            R.id.view5,
                            R.id.view6,
                            R.id.view7,
                            R.id.view8
                        )
                    }
                }
            }
        }
    }


}